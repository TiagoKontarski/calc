from kivy.app import App
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.lang.builder import Builder
from adm_telas import AdmTelas
from tela_inicio import TelaInicio

ARQ = Builder.load_file('ui/interface.kv')


class Main(App):
    def build(self):
        return AdmTelas()


if __name__ == '__main__':
    Main().run()
